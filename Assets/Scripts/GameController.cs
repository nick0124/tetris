﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//will be singleton
public class GameController : MonoBehaviour
{

    //класс с игровыми фигурами
    private Figures figures;
    //текущая фигура
    private Figure currentFigure;

    //поле с игровыми ячейками
    [SerializeField] private Field field = new Field();
    private DisplayArray presentArray;
    //класс с массивом для отображения
    [SerializeField] private FieldPresenter fieldPresenter;

    private float tickTimer;
    //скорость игры
    [SerializeField] private float gameSpeed;
    public float GameSpeed
    {
        get { return gameSpeed; }
        set { gameSpeed = value; }
    }
    private int score;
    public int Score { get { return score; } }

    [SerializeField] Text scoreTxt;

    [SerializeField] private bool secondGameMode;

    // Use this for initialization
    void Start()
    {
        figures = new Figures();
        SpawnFigure();
        presentArray = new DisplayArray(field, this);
    }

    // Update is called once per frame
    //TODO добавить обновление игры с помощью тиков
    void Update()
    {
        //TODO задать очеред выполнения операций, иногда взаимодействем с фигурой когда ее еще нет
        tickTimer += Time.deltaTime;
        if (tickTimer >= gameSpeed)
        {
            if (!FigureCollision.CollideWithBottomBorder(field, currentFigure))
                FigureMovement.MoveDown(currentFigure);
            tickTimer = 0;
        }

        //передвигаем фигуру
        if (Input.GetKeyDown(KeyCode.A))
            FigureController.MoveFigureLeft(field, currentFigure, secondGameMode);
        if (Input.GetKeyDown(KeyCode.D))
            FigureController.MoveFigureRight(field, currentFigure, secondGameMode);
        if (Input.GetKeyDown(KeyCode.S))
            FigureController.MoveFigureDown(field, currentFigure);

        if (Input.GetKeyDown(KeyCode.Q))
            FigureController.RotateFigureLeft(field, currentFigure);
        if (Input.GetKeyDown(KeyCode.E))
            FigureController.RotateFigureRight(field, currentFigure);

        //рисуем игровое поле
        fieldPresenter.DrawField(presentArray.GetPresentArray());

        //если не можем дфигать фмгуру ниже то устанавливаем фигуру и создаем новую
        if (!CanMoveDown(field, currentFigure))
        {
            SetupFigure(field, currentFigure);
            SpawnFigure();
        }

        //если второй режим игры
        if (secondGameMode)
        {
            //если количество собранных линий больше 2 то обновляем счет и удалаяем собранные линии
            if(LineCheck.NumberOfCompletedLines(field) == 2)
            {
                score += LineCheck.NumberOfCompletedLines(field);
                LineCheck.RemoveCompleteLines(field);
            }
        }
        //если первый режим игры
        else
        {
            //обновляем счет и удаляем собранные линии
            score += LineCheck.NumberOfCompletedLines(field);
            LineCheck.RemoveCompleteLines(field);
        }

        //обновляем массив для отрисовки
        presentArray.UpdatePresentArray(field);

        //обнолвяем счет
        scoreTxt.text = "Score: " + score;
    }

    public Figure GetCurrentFigure()
    {
        return currentFigure;
    }

    private bool CanMoveDown(Field gameField, Figure figure)
    {
        if (FigureCollision.CollideWithBottomBorder(gameField, figure) || !FigureCollision.CollideWithBottomCell(gameField, figure))
        {
            return false;
        }
        return true;
    }
    //TODO вынести в какой то другой класс, возможно в Figure, вынести SpawnFigure
    private void SetupFigure(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            gameField.field[figureCell.y, figureCell.x] = 1;
        }
    }

    //TODO вынести в какой то другой класс, возможно в тот же что вынесли SetupBlock
    private void SpawnFigure()
    {
        float[] dropChances;
        if (secondGameMode)
        {
            dropChances = new float[] { 0.1f, 0.15f, 0.15f, 0.15f, 0.15f, 0.1f, 0.05f, 0.05f, 0.05f, 0.05f };
        }
        else
        {
            dropChances = new float[] { 0.1f, 0.15f, 0.15f, 0.15f, 0.15f, 0.1f, 0.2f, 0f, 0f, 0f };
        }
        int dropFigureNumber = DropChance.Choose(dropChances);
        currentFigure = NewFigure(figures.GetFigure(dropFigureNumber), 4, 0);

        if(FinishGame(field,currentFigure))
        {
            SceneManager.LoadScene(1);
        }
    }

    //TODO перенести вконструктор фигуры
    private Figure NewFigure(Figure figure,int x,int y)
    {
        Figure newFigure = new Figure();
        newFigure.figureCells = new FigureCell[figure.figureCells.Length];
        for (int i = 0; i < newFigure.figureCells.Length; i++)
        {
            newFigure.figureCells[i] = new FigureCell();
            newFigure.figureCells[i].x = figure.figureCells[i].x + x;
            newFigure.figureCells[i].y = figure.figureCells[i].y + y;

            if (figure.rotationPivotCell.x + x == newFigure.figureCells[i].x && figure.rotationPivotCell.y + y == newFigure.figureCells[i].y)
                newFigure.rotationPivotCell = newFigure.figureCells[i];
        }

        return newFigure;
    }

    private bool FinishGame(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            if (gameField.field[figureCell.y, figureCell.x] == 1)
                return true;
        }
        return false;
    }
}
