﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayArray : MonoBehaviour {

    private Field field;
    private GameController gameController;

    private int[,] presentArray;

    // Use this for initialization
    public DisplayArray(Field field, GameController gameController) {
        this.field = field;
        this.gameController = gameController;
        presentArray = CreatePresentArray(this.field.field.GetLength(0), this.field.field.GetLength(1));
    }
	
    private int[,] CreatePresentArray(int rowsCount, int columnsCount)
    {
        int[,] fieldArray = new int[rowsCount, columnsCount];
        for (int row = 0; row < fieldArray.GetLength(0); row++)
        {
            for (int column = 0; column < fieldArray.GetLength(1); column++)
            {
                fieldArray[row, column] = 0;
            }
        }
        return fieldArray;
    }

    public int[,] GetPresentArray()
    {
        return presentArray;
    }

    public void UpdatePresentArray(Field field)
    {
        for (int y = 0; y < field.field.GetLength(0); y++)
        {
            for (int x = 0; x < field.field.GetLength(1); x++)
            {
                    presentArray[y, x] = 0;
            }
        }
        for (int y = 0; y < field.field.GetLength(0); y++)
        {
            for (int x = 0; x < field.field.GetLength(1); x++)
            {
                if(field.field[y,x] == 1)
                {
                    presentArray[y, x] = 1;
                }
            }
        }

        foreach (FigureCell figureCell in gameController.GetCurrentFigure().figureCells)
        {
            presentArray[figureCell.y, figureCell.x] = 1;
        }
    }
}
