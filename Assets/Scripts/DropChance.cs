﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropChance
{
    //TODO добавить описание как работает
    public static int Choose(float[] chanses)
    {
        float total = 0;
        foreach (float elem in chanses)
        {
            total += elem;
        }

        float randomPoint = Random.value * total;
        for (int i = 0; i < chanses.Length; i++)
        {
            if (randomPoint < chanses[i])
            {
                return i;
            }
            else
            {
                randomPoint -= chanses[i];
            }
        }
        return chanses.Length - 1;
    }
}
