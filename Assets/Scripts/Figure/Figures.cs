﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Figures : MonoBehaviour
{

    private Figure[] figures1;
    private Figure[] figures2;

    //exception if figure lengt or heigth larger then field

    int[,] figure1 = new int[,]
    {
        { 2,1},
        { 1,1}
    };

    int[,] figure2 = new int[,]
    {
        { 1,1,0},
        { 0,2,1}
    };
    int[,] figure3 = new int[,]
    {
        { 0,1,1},
        { 1,2,0}
    };
    int[,] figure4 = new int[,]
    {
        { 0,0,1},
        { 1,1,2}
    };
    int[,] figure5 = new int[,]
    {
        { 1,0,0},
        { 2,1,1}
    };
    int[,] figure6 = new int[,]
    {
        { 1,2,1,1}
    };
    int[,] figure7 = new int[,]
    {
        { 0,1,0},
        { 1,2,1}
    };

    int[,] figure8 = new int[,]
    {
        { 0,1,0},
        { 1,2,1},
        { 1,0,1}
    };

    int[,] figure9 = new int[,]
    {
        { 1,2,1},
        { 1,0,1}
    };
    int[,] figure10 = new int[,]
    {
        { 1,0,0},
        { 1,2,0},
        { 0,1,1}
    };


    public Figures()
    {
        figures1 = new Figure[10];
        figures1[0] = ArrayToFigure(figure1);
        figures1[1] = ArrayToFigure(figure2);
        figures1[2] = ArrayToFigure(figure3);
        figures1[3] = ArrayToFigure(figure4);
        figures1[4] = ArrayToFigure(figure5);
        figures1[5] = ArrayToFigure(figure6);
        figures1[6] = ArrayToFigure(figure7);
        figures1[7] = ArrayToFigure(figure8);
        figures1[8] = ArrayToFigure(figure9);
        figures1[9] = ArrayToFigure(figure10); 
    }

    private Figure ArrayToFigure(int[,] figureArray)
    {
        Figure figure = new Figure();

        int cellsCount = 0;
        for (int row = 0; row < figureArray.GetLength(0); row++)
        {
            for (int column = 0; column < figureArray.GetLength(1); column++)
            {
                if(figureArray[row, column] > 0)
                {
                    cellsCount++;
                }
            }
        }

        figure.figureCells = new FigureCell[cellsCount];
        int cellToFill = 0;
        for (int row = 0; row < figureArray.GetLength(0); row++)
        {
            for (int column = 0; column < figureArray.GetLength(1); column++)
            {
                if (figureArray[row, column] == 1)
                {
                    figure.figureCells[cellToFill] = new FigureCell();
                    figure.figureCells[cellToFill].x = column;
                    figure.figureCells[cellToFill].y = row;
                    cellToFill++;
                }
                else if (figureArray[row, column] == 2)
                {
                    figure.figureCells[cellToFill] = new FigureCell();
                    figure.figureCells[cellToFill].x = column;
                    figure.figureCells[cellToFill].y = row;

                    figure.rotationPivotCell = figure.figureCells[cellToFill];

                    cellToFill++;
                }
            }
        }

        return figure;
    }

    public Figure GetFigure(int figureNumber)
    {
        return figures1[figureNumber];
    }
}
