﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour {

    public FigureCell[] figureCells;

    public FigureCell rotationPivotCell;

    /*
    public Figure(int[,] figureArray)
    {

        Figure figure = ArrayToFigure(figureArray);
        figureCells = new FigureCell[figure.figureCells.Length];
        for (int i = 0; i < figure.figureCells.Length; i++)
        {
            figureCells[i] = new FigureCell();
            figureCells[i].x = figure.figureCells[i].x;
            figureCells[i].y = figure.figureCells[i].y;
        }
        rotationPivotCell = new FigureCell();
        rotationPivotCell.x = figure.rotationPivotCell.x;
        rotationPivotCell.y = figure.rotationPivotCell.y;
    }

    private Figure ArrayToFigure(int[,] figureArray)
    {
        Figure figure = new Figure(figureArray);

        int cellsCount = 0;
        for (int row = 0; row < figureArray.GetLength(0); row++)
        {
            for (int column = 0; column < figureArray.GetLength(1); column++)
            {
                if (figureArray[row, column] > 0)
                {
                    cellsCount++;
                }
            }
        }

        figure.figureCells = new FigureCell[cellsCount];
        int cellToFill = 0;
        for (int row = 0; row < figureArray.GetLength(0); row++)
        {
            for (int column = 0; column < figureArray.GetLength(1); column++)
            {
                if (figureArray[row, column] == 1)
                {
                    figure.figureCells[cellToFill] = new FigureCell();
                    figure.figureCells[cellToFill].x = column;
                    figure.figureCells[cellToFill].y = row;
                    cellToFill++;
                }
                else if (figureArray[row, column] == 2)
                {
                    figure.figureCells[cellToFill] = new FigureCell();
                    figure.figureCells[cellToFill].x = column;
                    figure.figureCells[cellToFill].y = row;

                    figure.rotationPivotCell = new FigureCell();
                    figure.rotationPivotCell.x = column;
                    figure.rotationPivotCell.y = row;
                    cellToFill++;
                }
            }
        }

        return figure;
        //Debug.Log(figure);
    }
    */
}
