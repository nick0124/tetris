﻿using UnityEngine;

/// <summary>
/// Класс для обнаружения столкновений
/// </summary>
public class FigureCollision{

    #region Check to collision when move
    /// <summary>
    /// Метод проверяет столкнулись ли с правой границцей игрового поля
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CollideWithRightBorder(Field gameField, Figure figure)
    {
        //проверяем все ячейки фигуры
        foreach (FigureCell figureCell in figure.figureCells)
        {
            //если ячейка фигуры находится в последнем столбце игрового поля значит столкнулись
            if (figureCell.x == gameField.field.GetLength(1)-1)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// Метод проверяет столкнулись ли с левой границцей игрового поля
    /// </summary>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CollideWithLeftBorder(Figure figure)
    {
        //проверяем все ячейки фигуры
        foreach (FigureCell figureCell in figure.figureCells)
        {
            //если ячейка фигуры находится в первом столбце игрового поля значит столкнулись
            if (figureCell.x == 0)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// Метод проверяет столкнулись ли с нижней границцей игрового поля
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CollideWithBottomBorder(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            //если ячейка фигуры находится в последней линии игрового поля значит столкнулись
            if (figureCell.y == gameField.field.GetLength(0)-1)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Метод проверяет столкнулись ли с заполненной ячейкой справа от фигуры
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CollideWithRightCell(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            if (figureCell.x + 1 < gameField.field.GetLength(1) - 1)
            {
                if (gameField.field[figureCell.y, figureCell.x + 1] == 1)
                {
                    return false;
                }
            }
        }
        return true;
    }
    /// <summary>
    /// Метод проверяет столкнулись ли с заполненной ячейкой слева от фигуры
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CollideWithLeftCell(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            if (figureCell.x - 1 > 0)
            {
                if (gameField.field[figureCell.y, figureCell.x - 1] == 1)
                {
                    return false;
                }
            }
        }
        return true;
    }
    /// <summary>
    /// Метод проверяет столкнулись ли с заполненной ячейкой снизу от фигуры
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CollideWithBottomCell(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            if (gameField.field[figureCell.y + 1, figureCell.x] == 1)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Метод проверяет можем ли передвинуть фигуру через правую границу
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CanMoveTrouthRightBorder(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            if (figureCell.x + 1 > gameField.field.GetLength(1) - 1)
            {
                int x = figureCell.x - gameField.field.GetLength(1)+1;
                if (gameField.field[figureCell.y, x] == 1)
                {
                    return false;
                }
            }
        }
        return true;
    }
    /// <summary>
    /// Метод проверяет можем ли передвинуть фигуру через левую границу
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CanMoveTrouthLeftBorder(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            if (figureCell.x - 1 < 0)
            {
                int x = figureCell.x + gameField.field.GetLength(1)-1;
                if (gameField.field[figureCell.y, x] == 1)
                {
                    return false;
                }
            }
        }
        return true;
    }
    #endregion

    #region Check to collision when rotate
    /// <summary>
    /// Метод проверяет можем ли повернуть фигуру по часовой стрелки
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CanRotateRight(Field gameField, Figure figure)
    {

        FigureCell[] collisionCheckFigure = new FigureCell[figure.figureCells.Length];

        Vector2 center = new Vector2(figure.rotationPivotCell.x, figure.rotationPivotCell.y);

        for (int i = 0; i < figure.figureCells.Length; i++)
        {
            collisionCheckFigure[i] = new FigureCell();
            collisionCheckFigure[i].x = figure.figureCells[i].x;
            collisionCheckFigure[i].y = figure.figureCells[i].y;

            Vector2 RelativeVector = Vector2.zero;
            Vector2 TransformedVector = Vector2.zero;

            //find relative vector
            RelativeVector.x = figure.figureCells[i].x - center.x;
            RelativeVector.y = figure.figureCells[i].y - center.y;

            //find transformed vector
            TransformedVector.x = 0 * RelativeVector.x + -1 * RelativeVector.y;
            TransformedVector.y = 1 * RelativeVector.x + 0 * RelativeVector.y;

            //find rotated position
            collisionCheckFigure[i].x = (int)(center.x + TransformedVector.x);
            collisionCheckFigure[i].y = (int)(center.y + TransformedVector.y);
        }


        foreach (FigureCell testFigureCell in collisionCheckFigure)
        {
            if (testFigureCell.y < 0 || testFigureCell.y > gameField.field.GetLength(0) - 1)
            {
                return false;
            }
            else if (testFigureCell.x < 0 || testFigureCell.x > gameField.field.GetLength(1) - 1)
            {
                return false;
            }
            else if (gameField.field[testFigureCell.y, testFigureCell.x] == 1)
            {
                return false;
            }
        }
        return true;
    }
    /// <summary>
    /// Метод проверяет можем ли повернуть фигуру по против часовой стрелки
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="figure">Фигура у которой проверяем стлкновения</param>
    /// <returns>Столкнулись или нет</returns>
    public static bool CanRotateLeft(Field gameField, Figure figure)
    {
        FigureCell[] collisionCheckFigure = new FigureCell[figure.figureCells.Length];

        Vector2 center = new Vector2(figure.rotationPivotCell.x, figure.rotationPivotCell.y);

        for (int i = 0; i < figure.figureCells.Length; i++)
        {
            collisionCheckFigure[i] = new FigureCell();
            collisionCheckFigure[i].x = figure.figureCells[i].x;
            collisionCheckFigure[i].y = figure.figureCells[i].y;

            Vector2 RelativeVector = Vector2.zero;
            Vector2 TransformedVector = Vector2.zero;

            //find relative vector
            RelativeVector.x = figure.figureCells[i].x - center.x;
            RelativeVector.y = figure.figureCells[i].y - center.y;

            //find transformed vector
            TransformedVector.x = 0 * RelativeVector.x + 1 * RelativeVector.y;
            TransformedVector.y = -1 * RelativeVector.x + 0 * RelativeVector.y;

            //find rotated position
            collisionCheckFigure[i].x = (int)(center.x + TransformedVector.x);
            collisionCheckFigure[i].y = (int)(center.y + TransformedVector.y);
        }


        foreach (FigureCell testFigureCell in collisionCheckFigure)
        {
            if (testFigureCell.y < 0 || testFigureCell.y > gameField.field.GetLength(0) - 1)
            {
                return false;
            }
            else if (testFigureCell.x < 0 || testFigureCell.x > gameField.field.GetLength(1) - 1)
            {
                return false;
            }
            else if (gameField.field[testFigureCell.y, testFigureCell.x] == 1)
            {
                return false;
            }
        }
        return true;
    }
    #endregion
}
