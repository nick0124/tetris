﻿using UnityEngine;

/// <summary>
/// Класс для взаимодействия с линиями игрового поля
/// </summary>
public class LineCheck {

    /// <summary>
    /// Метод проверяет число заполненных линий
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <returns>Количество заполненных линий</returns>
    public static int NumberOfCompletedLines(Field gameField)
    {
        int completedLinesCount = 0;
        for (int lineNumber = 0; lineNumber < gameField.field.GetLength(0); lineNumber++)
        {
            if (LineIsComplete(gameField, lineNumber))
            {
                completedLinesCount++;
            }
        }
        return completedLinesCount;
    }

    /// <summary>
    /// Метод удаляет заполненные линии
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    public static void RemoveCompleteLines(Field gameField)
    {
        //проверяем каждую линию
        for (int lineNumber = 0; lineNumber < gameField.field.GetLength(0); lineNumber++)
        {
            //если линия собрана
            if (LineIsComplete(gameField, lineNumber))
            {
                //удаляем линию
                RemoveLine(gameField, lineNumber);
                //сдвигаем все линии до удаленной линии вниз
                MoveLinesDown(gameField, lineNumber);
            }
        }
    }
    /// <summary>
    /// Метод проверяет заполнена ли линия
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="lineNumber">Номер линии для проверки</param>
    /// <returns>Собрана линия или нет</returns>
    private static bool LineIsComplete(Field gameField, int lineNumber)
    {
        for (int columnNumber = 0; columnNumber < gameField.field.GetLength(1); columnNumber++)
        {
            if (gameField.field[lineNumber, columnNumber] == 0)
            {
                return false;
            }
        }
        return true;
    }
    /// <summary>
    /// Метод удаляет указанную линию
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="lineNumber">Номер удаляемой линии</param>
    private static void RemoveLine(Field gameField, int lineNumber)
    {
        for (int columnNumber = 0; columnNumber < gameField.field.GetLength(1); columnNumber++)
        {
            gameField.field[lineNumber, columnNumber] = 0;
        }
    }
    /// <summary>
    /// Сдвигает все линии до указанной линии на 1 клетку вниз
    /// </summary>
    /// <param name="gameField">Поле с игровыми ячейками</param>
    /// <param name="completedLineNumber">Номер линии до которой сдвигаем другие линии</param>
    private static void MoveLinesDown(Field gameField, int completedLineNumber)
    {
        for (int lineNumber = completedLineNumber; lineNumber > 0; lineNumber--)
        {
            for (int columnNumber = 0; columnNumber < gameField.field.GetLength(1); columnNumber++)
            {
                gameField.field[lineNumber, columnNumber] = gameField.field[lineNumber - 1, columnNumber];
                gameField.field[lineNumber - 1, columnNumber] = 0;
            }
        }
    }
}
