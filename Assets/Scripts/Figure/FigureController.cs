﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс для управления фигурами
/// </summary>
public class FigureController : MonoBehaviour {

    public static void MoveFigureRight(Field field, Figure currentFigure, bool canMoveTrouthBorder)
    {
        if (!canMoveTrouthBorder)
        {
            if (!FigureCollision.CollideWithRightBorder(field, currentFigure))
                if (FigureCollision.CollideWithRightCell(field, currentFigure))
                    FigureMovement.MoveRight(currentFigure);
        }
        else
        {
            if (FigureCollision.CanMoveTrouthRightBorder(field, currentFigure))
                if (FigureCollision.CollideWithRightCell(field, currentFigure))
                {
                    FigureMovement.MoveRight(currentFigure);
                    FigureMovement.MoveTrouthBorder(field, currentFigure);
                }
        }
    }
    public static void MoveFigureLeft(Field field, Figure currentFigure, bool canMoveTrouthBorder)
    {
        if (!canMoveTrouthBorder)
        {
            if (!FigureCollision.CollideWithLeftBorder(currentFigure))
                if (FigureCollision.CollideWithLeftCell(field, currentFigure))
                    FigureMovement.MoveLeft(currentFigure);
        }
        else
        {
            if (FigureCollision.CanMoveTrouthLeftBorder(field, currentFigure))
                if (FigureCollision.CollideWithLeftCell(field, currentFigure))
                {
                    FigureMovement.MoveLeft(currentFigure);
                    FigureMovement.MoveTrouthBorder(field, currentFigure);
                }
        }
    }
    public static void MoveFigureDown(Field field, Figure currentFigure)
    {
        if (!FigureCollision.CollideWithBottomBorder(field, currentFigure))
            FigureMovement.MoveDown(currentFigure);
    }

    public static void RotateFigureLeft(Field field, Figure currentFigure)
    {
        if (FigureCollision.CanRotateLeft(field, currentFigure))
            FigureRotation.RotateLeft(currentFigure);
    }
    public static void RotateFigureRight(Field field, Figure currentFigure)
    {
        if (FigureCollision.CanRotateRight(field, currentFigure))
            FigureRotation.RotateRight(currentFigure);
    }
}
