﻿using UnityEngine;

public class FigureRotation{

    public static void RotateRight(Figure figure)
    {
        Vector2 center = new Vector2(figure.rotationPivotCell.x, figure.rotationPivotCell.y);

        foreach (FigureCell figureCell in figure.figureCells)
        {
            Vector2 relativeVector = Vector2.zero;
            Vector2 transformedVector = Vector2.zero;

            //find relative vector
            relativeVector.x = figureCell.x - center.x;
            relativeVector.y = figureCell.y - center.y;

            //find transformed vector
            transformedVector.x = 0 * relativeVector.x + -1 * relativeVector.y;
            transformedVector.y = 1 * relativeVector.x + 0 * relativeVector.y;

            //find rotated position
            figureCell.x = (int)(center.x + transformedVector.x);
            figureCell.y = (int)(center.y + transformedVector.y);
        }
    }
    public static void RotateLeft(Figure figure)
    {
        Vector2 center = new Vector2(figure.rotationPivotCell.x, figure.rotationPivotCell.y);

        foreach (FigureCell figureCell in figure.figureCells)
        {
            Vector2 relativeVector = Vector2.zero;
            Vector2 transformedVector = Vector2.zero;

            //find relative vector
            relativeVector.x = figureCell.x - center.x;
            relativeVector.y = figureCell.y - center.y;

            //find transformed vector
            transformedVector.x = 0 * relativeVector.x + 1 * relativeVector.y;
            transformedVector.y = -1 * relativeVector.x + 0 * relativeVector.y;

            //find rotated position
            figureCell.x = (int)(center.x + transformedVector.x);
            figureCell.y = (int)(center.y + transformedVector.y);
        }
    }
}
