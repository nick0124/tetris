﻿using UnityEngine;

public class FigureMovement {

    public static void MoveDown(Figure figure)
    {
        for (int i = 0; i < figure.figureCells.Length; i++)
        {
            figure.figureCells[i].y++;
        }
    }
    public static void MoveRight(Figure figure)
    {
        for (int i = 0; i < figure.figureCells.Length; i++)
        {
            figure.figureCells[i].x++;
        }
    }
    public static void MoveLeft(Figure figure)
    {
        for (int i = 0; i < figure.figureCells.Length; i++)
        {
            figure.figureCells[i].x--;
        }
    }

    public static void MoveTrouthBorder(Field gameField, Figure figure)
    {
        foreach (FigureCell figureCell in figure.figureCells)
        {
            //out of right border
            if (figureCell.x > gameField.field.GetLength(1) - 1)
            {
                figureCell.x = figureCell.x - gameField.field.GetLength(1);
            }
            //out of left border
            if (figureCell.x < 0)
            {
                figureCell.x = figureCell.x + gameField.field.GetLength(1);
            }
        }
    }
}
