﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldPresenter : MonoBehaviour {

    //prefab to show cell on screen
    [SerializeField] private GameObject cellPrefub;
    //array of cells of field
    private FieldCell[,] fieldCells;

    private bool arrayIsInit = false;

    public void DrawField(int[,] fieldArray)
    {
        if(arrayIsInit == false)
            InitField(fieldArray);
        if (fieldCells != null)
            Draw(fieldArray);
    }

    private void InitField(int[,] filedArray)
    {
        //init array of field cells
        fieldCells = new FieldCell[filedArray.GetLength(0), filedArray.GetLength(1)];

        //fill array with cells prefubs
        for (int y = 0; y < filedArray.GetLength(0); y++)
        {
            for (int x = 0; x < filedArray.GetLength(1); x++)
            {
                GameObject cellPrefubInstance = Instantiate(cellPrefub);
                cellPrefubInstance.transform.position = new Vector3(x, -y, 0);
                cellPrefubInstance.transform.parent = gameObject.transform;
                cellPrefubInstance.transform.name = "Cell[" + x + "," + y + "]";
                fieldCells[y, x] = cellPrefubInstance.GetComponent<FieldCell>();
            }
        }
    }

    private void Draw(int[,] fieldArray)
    {
        for (int y = 0; y < fieldArray.GetLength(0); y++)
        {
            for (int x = 0; x < fieldArray.GetLength(1); x++)
            {
                if (fieldArray[y, x] == 0)
                    fieldCells[y, x].FilledCell(false);
                else
                    fieldCells[y, x].FilledCell(true);
            }
        }
    }
}
