﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ячейка для отображения игрового поля
/// </summary>
public class FieldCell : MonoBehaviour
{

    private int color;

    [SerializeField] private SpriteRenderer spritrRenderer;

    /// <summary>
    /// Окрашивает ячейку если заполненна
    /// </summary>
    /// <param name="colored">Заполненна ячейка или нет</param>
    public void FilledCell(bool colored)
    {
        if (colored)
            spritrRenderer.color = Color.black;
        else
            spritrRenderer.color = Color.white;
    }
}