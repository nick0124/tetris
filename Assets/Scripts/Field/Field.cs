﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{

    [SerializeField] private int columns;
    [SerializeField] private int rows;


    public int[,] field;

    private void Awake()
    {
        field = CreateField(rows, columns);
    }

    private int[,] CreateField(int rowsCount, int columnsCount)
    {
        int[,] fieldArray = new int[rowsCount, columnsCount];
        for (int row = 0; row < fieldArray.GetLength(0); row++)
        {
            for (int column = 0; column < fieldArray.GetLength(1); column++)
            {
                fieldArray[row, column] = 0;
            }
        }
        return fieldArray;
    }
}
