﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawField : MonoBehaviour {

    
    //filled cells
    public Field field;

    //prefab to show cell on screen
    public GameObject cellPrefub;
    //array of cells of field
    public FieldCell[,] fieldCells;

	// Use this for initialization
	void Start () {
        //GetLength(0); rows 
        //GetLength(1); columns
        //Array[y,x] y - row x - column
        InitField(field.field);
    }
	
	// Update is called once per frame
	void Update () {
        UpdateField(field.field);
	}

    private void InitField(int[,] filedArray)
    {
        //init array of field cells
        fieldCells = new FieldCell[filedArray.GetLength(0), filedArray.GetLength(1)];

        //fill array with cells prefubs
        for (int y = 0; y < filedArray.GetLength(0); y++)
        {
            for (int x = 0; x < filedArray.GetLength(1); x++)
            {
                GameObject cellPrefubInstance = Instantiate(cellPrefub);
                cellPrefubInstance.transform.position = new Vector3(x, -y, 0);
                cellPrefubInstance.transform.parent = gameObject.transform;
                cellPrefubInstance.transform.name = "Cell[" + x + "," + y + "]";
                fieldCells[y, x] = cellPrefubInstance.GetComponent<FieldCell>();
            }
        }
    }

    private void UpdateField(int[,] fieldArray)
    {
        for (int y = 0; y < fieldArray.GetLength(0); y++)
        {
            for (int x = 0; x < fieldArray.GetLength(1); x++)
            {
                if (fieldArray[y, x] == 0)
                    fieldCells[y, x].FilledCell(false);
                else
                    fieldCells[y, x].FilledCell(true);
            }
        }
    }
}
