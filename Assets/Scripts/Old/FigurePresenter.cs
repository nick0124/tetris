﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigurePresenter : MonoBehaviour {

    public GameObject[] cellPrefubs;
    public GameObject cellPrefub;

    public void InitNewFigure(Figure figure)
    {
        cellPrefubs = new GameObject[figure.figureCells.Length];
        for (int i = 0; i < cellPrefubs.Length; i++)
        {
            cellPrefubs[i] = Instantiate(cellPrefub);
        }
    }

    public void DrawFigureOnField(Figure figure)
    {
        for (int i = 0; i < figure.figureCells.Length; i++)
        {
            cellPrefubs[i].transform.position = new Vector3(figure.figureCells[i].x, -figure.figureCells[i].y, -1);
        }
    }

    public void RemoveFigure()
    {
        foreach (GameObject cellPrefub in cellPrefubs)
        {
            GameObject.Destroy(cellPrefub);
        }
        cellPrefubs = null;
    }
}
